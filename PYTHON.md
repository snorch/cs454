#Python

##Link to pygame example repo [pygame-first](https://github.com/senya/pygame-first)

##How to install Python 3.5 + pygame on Windows x86_64

0) If you had python/pygame installed before - remove them with uninstaller!

1) Download latest(3.5) python from: [python-3.5.1-amd64-webinstall.exe](https://www.python.org/ftp/python/3.5.1/python-3.5.1-amd64-webinstall.exe) and install(by default) to directory:

    %APPDATA%\..\Local\Programs\Python\Python35\

2) Download latest(1.9.1 for python 3.5) pygame from: [pygame-1.9.2a0-cp35-none-win_amd64.whl](http://www.lfd.uci.edu/~gohlke/pythonlibs/th4jbnf9/pygame-1.9.2a0-cp35-none-win_amd64.whl)
Put it to:

    %APPDATA%\..\Local\Programs\Python\Python35\Scripts

3) Open comand line: `Win+r -> enter "cmd" -> Enter`. And then change directory:

    cd %APPDATA%\..\Local\Programs\Python\Python35\Scripts

4) Run command to install pygame:

    pip3 install pygame-1.9.2a0-cp35-none-win_amd64.whl

5) Check pygame works:

    import pygame
    import pygame.examples.aliens as game
    game.main()

note: Unfortunately there are problems then you install combinations of python interpreter version and pygame library compiled with other version, or even for other hardware architecture e.g. x86 and x86_64 are not compatible. So install exactly same versions only!
